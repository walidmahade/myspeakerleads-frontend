<!-- include hader -->
<?php include './views/layout/header.php'; ?>
<!-- end header -->

<section id="pricing" class="section section--pricing">
    <div class="container">
        <p class="section-title tc fs-30 f-300" style="opacity: .8">
            Privacy Policy
        </p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet beatae est iste magni, numquam soluta totam ullam vero vitae voluptatem. Distinctio dolor eum labore maiores placeat quidem similique suscipit temporibus.</p>
    </div>
</section>

<style>
    .section--pricing {
        padding: 120px 0;
        margin: 100px 0;
    }
    html, body, p, h2,h3,h1,h4 {
        line-height: 1.6;
        font-size: 18px;
    }

    p {
        margin-bottom: 20px;
    }
</style>

<!-- include footer -->
<?php include './views/layout/footer.php'; ?>
<!-- end footer -->