<!-- include hader -->
<?php include './views/layout/header.php'; ?>
<!-- end header -->
<?php if ($has_error) { ?>
    <section id="pricing" class="section section--pricing tc">
        <div class="container">
            <p class="section-title tc fs-20 f-300" style="opacity: .8">
                Oops! Sorry something went wrong.
            </p>
            <p class="fs-25">Make sure you have sufficient balance &</p>
            <br>
            <p class="fs-25">Double check your payment info</p>
            <br><br>
            <div>
                <a href="/signup" class="btn btn--primary">Try Again</a> 
                <span style="padding: 0 15px;">or </span>
                <a href="/contact" class="text-link">Contact Us</a>
            </div>
        </div>
    </section>
<?php } else { ?>
    <section id="pricing" class="section section--pricing tc">
        <div class="container">
            <p class="section-title tc fs-20 f-300" style="opacity: .8">
                Your registration was successful.
            </p>
            <p><b class="fs-30">Please check your email for login info.</b></p>
        </div>
    </section>
<?php } ?>

    <style>
        .section--pricing {
            padding: 120px 0;
            margin: 100px 0;
        }
    </style>

<!-- include footer -->
<?php include './views/layout/footer.php'; ?>
<!-- end footer -->