<?php
    $req_uri = ucfirst(
        ltrim($_SERVER['REQUEST_URI'], '/')
    );
    $page_title = '';

    if (strlen($req_uri) > 0) {
        $page_title = '| ' . $req_uri;
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport"content="width=device-width, initial-scale=1.0"> <meta http-equiv="X-UA-Compatible"content="ie=edge">
    <link rel="shortcut icon" href="/static/img/forfavicon.png" type="image/x-icon">
    <title>My Speaker Leads <?php echo $page_title; ?></title>
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,500,400,600,700|Rubik:300,400,500" rel="stylesheet">
	<!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">-->
	<link rel="stylesheet" href="/static/css/owl.carousel.min.css">
	<link rel="stylesheet" href="/static/css/style.css">
</head>

<body>
	<header class="site-header">
		<div class="container">
			<section class="brand ">
                <a href="/">
                    <img src="/static/img/msl-logo.png" alt="">
                    <span>MySpeakerLeads</span>
                </a>
			</section>
			<nav class="main-menu">
				<a href="/#hero" class="main-menu__item">Home</a>
				<a href="/#features" class="main-menu__item">Features</a>
				<a href="/#pricing" class="main-menu__item">Pricing</a>
				<a href="/contact" target="_top" class="main-menu__item">Contact</a>
			</nav>
			<a href="/signup" class="btn btn--primary">Sign Up</a>
		</div>
	</header>