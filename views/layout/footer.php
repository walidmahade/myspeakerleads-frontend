
	<footer class="section--footer tc">
		<div class="container top">
			<div class="brand">
				<h1>My Speaker Leads</h1>
			</div>

			<div class="more-links">
				<h3 class="main-menu__item">Quick Links</h3>
				<div class="links">
					<a href="/#hero" class="main-menu__item">Home</a>
					<a href="/#features" class="main-menu__item">Features</a>
					<a href="/#pricing" class="main-menu__item">Pricing</a>
					<a href="/contact" class="main-menu__item">Contact</a>
				</div>
			</div>

			<div class="more-links">
				<h3 class="main-menu__item">Get Speaking Leads</h3>
				<div class="links">
					<a href="/signup" class="main-menu__item">Sign Up</a>
					<a href="/#features" class="main-menu__item">Features</a>
                    <a href="/contact" class="main-menu__item">Contact</a>
<!--					<a href="#faq" class="main-menu__item">FAQ</a>-->
				</div>
			</div>

			<div class="more-links">
				<h3 class="main-menu__item">Learn More</h3>
				<div class="links">
					<a href="/privacy" class="main-menu__item">Privacy Policy</a>
					<a href="/terms" class="main-menu__item">Terms & conditions</a>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="copy tc">All rights reserved © 2019</div>
		</div>
	</footer>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<script src="./static/js/owl.carousel.min.js"></script>
	<script src="./static/js/main.js"></script>
</body>

</html>