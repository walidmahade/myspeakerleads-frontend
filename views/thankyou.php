<?php
require_once('./views/config.php');

require_once("./vendor/autoload.php");
\Stripe\Stripe::setApiKey($stripe['secret_key']);

if ( !isset($_POST['stripeToken']) ) {
    header("Location: /");
    exit();
}

$token  = $_POST['stripeToken'];
$email  = $_POST['stripeEmail'];
$name  = $_POST['customerFirstName'] . $_POST['customerLastName'];
$price  = $_POST['price'];
$has_error = false;

// echo "<pre>";
// var_dump($_POST);
// echo "</pre>";


try {
  // Use Stripe's library to make requests...
$plan = \Stripe\Plan::create(array(
    "product" => [
        "name" => "My Speaker Leads",
        "type" => "service"
    ],
    "nickname" => "My Speaker Leads",
    "interval" => "month",
    "interval_count" => "1",
    "currency" => "usd",
    "amount" => $price,
));

  $customer = \Stripe\Customer::create([
      'email' => $email,
      'name' => $name,
      'source'  => $token,
  ]);

  $subscription = \Stripe\Subscription::create(array(
    "customer" => $customer->id,
    "items" => array(
        array(
            "plan" => $plan->id,
        ),
    ),
  ));

/*
{
    "FirstName":"Cook",
    "LastName":"Tim",
    "Email":"Cook@gmail.com",
    "StripeToken":"asdas90dasda98s",
    "SubscriptionStatus":"active",
    "SubscriptionId":"3",
    "CustomerId":"343434",
    "Amount": 19.99
}
 */


     $customerData = array(
         "FirstName" => $_POST['customerFirstName'],
         "LastName" => $_POST['customerLastName'],
         "Email" => $email  = $_POST['stripeEmail'],
         "StripeToken" => $_POST['stripeToken'],
         "SubscriptionStatus" => $subscription['status'],
         "SubscriptionId" => $subscription['id'],
         "CustomerId" => $customer['id'],
         "Amount" => $subscription['plan']['amount']
     );

// var_dump(json_encode($customerData));

     $options = array (
         'http' => array(
             'method'  => 'POST',
             'content' => json_encode( $customerData ),
             'header'=>  "Content-Type: application/json\r\n" .
                 "Accept: application/json\r\n"
         )
     );

     $url = 'http://app.myspeakerleads.com/api/NewUsers/PostNewUser';

     $context  = stream_context_create( $options );
     $result = file_get_contents( $url, false, $context );
     $response = json_decode( $result );

//     echo "<pre>";
//     var_dump($result);
//     echo "</pre>";

} catch(\Stripe\Error\Card $e) {
  // Since it's a decline, \Stripe\Error\Card will be caught
  $body = $e->getJsonBody();
  $err  = $body['error'];
  $has_error = true;

  // print('Status is:' . $e->getHttpStatus() . "\n");
  // print('Type is:' . $err['type'] . "\n");
  // print('Code is:' . $err['code'] . "\n");
  // param is '' in this case
  // print('Param is:' . $err['param'] . "\n");
  // print('Message is:' . $err['message'] . "\n");
} catch (\Stripe\Error\RateLimit $e) {
  // Too many requests made to the API too quickly
    $has_error = true;
} catch (\Stripe\Error\InvalidRequest $e) {
  // Invalid parameters were supplied to Stripe's API
    $has_error = true;
} catch (\Stripe\Error\Authentication $e) {
  // Authentication with Stripe's API failed
  // (maybe you changed API keys recently)
    $has_error = true;
} catch (\Stripe\Error\ApiConnection $e) {
  // Network communication with Stripe failed
    $has_error = true;
} catch (\Stripe\Error\Base $e) {
  // Display a very generic error to the user, and maybe send
  // yourself an email
    $has_error = true;
} catch (Exception $e) {
  // Something else happened, completely unrelated to Stripe
    $has_error = true;
}

include 'confirmation.php';

echo "<pre>";
var_dump($result);
echo "</pre>";