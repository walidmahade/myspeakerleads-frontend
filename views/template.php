<!-- include hader -->
<?php include './views/layout/header.php'; ?>
<!-- end header -->

<section id="pricing" class="section section--pricing tc">
    <div class="container">
        <p class="section-title tc fs-20 f-300" style="opacity: .8">
            Blank Page template
        </p>
        <p><b class="fs-30">Template content and stuff</b></p>
    </div>
</section>

<style>
    .section--pricing {
        padding: 120px 0;
        margin: 100px 0;
    }
</style>

<!-- include footer -->
<?php include './views/layout/footer.php'; ?>
<!-- end footer -->