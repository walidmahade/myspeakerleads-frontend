<?php require_once('./views/config.php'); ?>
<!-- include hader -->
<?php include './views/layout/header.php'; ?>
<!-- end header -->
<style>
  .StripeElement {
    box-sizing: border-box;
    height: 40px;
    padding: 10px 12px;
    border: 1px solid transparent;
    border-radius: 4px;
    background-color: white;
    box-shadow: 0 1px 3px 0 #e6ebf1;
    -webkit-transition: box-shadow 150ms ease;
    transition: box-shadow 150ms ease;
    font-family: inherit;
        border: 2px solid #5333ed;
  }
  .StripeElement--focus {
    box-shadow: 0 1px 3px 0 #cfd7df;
  }
  .StripeElement--invalid {
    border-color: #fa755a;
  }
  .StripeElement--webkit-autofill {
    background-color: #fefde5 !important;
  }
</style>

<script src="https://js.stripe.com/v3/"></script>

<section class="section section--payment">
  <div class="container">
      <div class="tc f-300">
          <h3 class="fs-30 f-300">Sign up for <b>$19.99</b> <span class="fs-14">/ month</span></h3>
          <br>
          <br>
          <p>Secure payments with stripe</p>
          <br>
          <br>
          <br>
      </div>

    <form action="/thankyou" method="post" id="payment-form">
        <div class="form-row-names">
            <div class="col">
                <label for="first-name">
                    First Name
                </label>
                <input id="first-name" class="form-control" name="customerFirstName" placeholder="Jenny" required>
            </div>

            <div class="col">
                <label for="name">
                    Last Name
                </label>
                <input id="name" class="form-control" name="customerLastName" placeholder="Rosen" required>
            </div>
        </div>

      <div class="form-row">
        <div class="form-row inline">
          <div class="col">
            <label for="email">
              Email Address
            </label>
            <input id="email" class="form-control" name="stripeEmail" type="email" placeholder="jenny.rosen@example.com" required>
          </div>
        </div>

        <label for="card-element">
          Credit or debit card
        </label>
        <div id="card-element">
        <!-- A Stripe Element will be inserted here. -->
        </div>

        <!-- Used to display form errors. -->
        <div id="card-errors" role="alert"></div>
      </div>

        <div class="cta cta--checkbox">
            <label for="agree-terms">
                <input type="checkbox" id="agree-terms" required>
                <span class="details-checkbox">I agree to the <a href="/terms" target="_blank" class="text-link">Terms & Conditions</a> of myspeakerleads.com</span>
            </label>
        </div>

      <div class="cta tc">
        <button class="btn btn--primary">Submit Payment</button>
      </div>

      <div id="preloader" class="tc"></div>
    </form>
  </div>
</section>

<script>
// Create a Stripe client.
// matts apikey
var stripe = Stripe('pk_test_GF3uXnw8UEQe6haHcIvGUH9w00Swtetfbo');
// var stripe = Stripe('pk_test_ID0QZLWEeJNS8GLq8oGgEESc'); // mahade api key
// Create an instance of Elements.
var elements = stripe.elements();
// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};
// Create an instance of the card Element.
var card = elements.create('card', {style: style});
// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');
// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});
let custData = {
    name: 'Firstname Lastname',
    address_line1: '21 Great Street',
    address_line2: 'Shilloong',
    address_city: 'Chicago',
    address_state: 'Illinois',
    address_zip: '12345',
    address_country: 'US'
};

// Handle form submission.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();
  // custom - show preloader
  var preloader = document.getElementById('preloader')
  preloader.classList.add('show')
  // end - show preloader

  stripe.createToken(card, custData).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

// Submit the form with the token ID.
function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

    // get price
  // var xhr = new XMLHttpRequest();
  // xhr.onreadystatechange = function() {
    // if (this.readyState == 4 && this.status == 200) {
      // var data = JSON.parse(this.responseText);
      // document.getElementById("price-field").value = data.Price * 100;
      // console.log(data.Price);
      // alert(data.Price);
      var hiddenInput2 = document.createElement('input');
      hiddenInput2.setAttribute('type', 'hidden');
      hiddenInput2.setAttribute('name', 'price');
      // hiddenInput2.setAttribute('value', Math.round(data.Price * 100));
      hiddenInput2.setAttribute('value', '1999');
      form.appendChild(hiddenInput2);

      // Submit the form
      form.submit();
    // }
  // };
  // xhr.open("GET", "https://cors.io/?http://app.myspeakerleads.com/api/Price/GetPrice", true);
  // xhr.send();
  // end get price
}
</script>

<!-- include footer -->
<?php include './views/layout/footer.php'; ?>
<!-- end footer -->