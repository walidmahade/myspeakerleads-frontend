<?php
$request = trim($_SERVER['REQUEST_URI'], '/');
//var_dump($request);

switch ($request) {
    case '/' :
        require __DIR__ . '/views/index.php';
        break;
    case '' :
        require __DIR__ . '/views/index.php';
        break;
    case 'contact' :
        require __DIR__ . '/views/contact.php';
        break;
    case 'signup' :
        require __DIR__ . '/views/signup.php';
        break;
    case 'thankyou' :
        require __DIR__ . '/views/thankyou.php';
        break;
    case 'terms' :
        require __DIR__ . '/views/terms.php';
        break;
    case 'privacy' :
        require __DIR__ . '/views/privacy-policy.php';
        break;
    case 'template' :
        require __DIR__ . '/views/template.php';
        break;
    default:
        require __DIR__ . '/views/404.php';
        break;
}